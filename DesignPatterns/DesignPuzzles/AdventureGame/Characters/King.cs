using DesignPatterns.DesignPuzzles.AdventureGame.ArmorBehaviors;
using DesignPatterns.DesignPuzzles.AdventureGame.WeaponBehaviors;

namespace DesignPatterns.DesignPuzzles.AdventureGame.Characters
{
    public class King : Character
    {
        public King()
        {
            SetWeapon(new Sword());
            SetArmor(new ChainMail());
            SetStats()
        }

        public string GetRank() => "I'm the king!";
        public string Swing() => $"Swong with a {Weapon.Damage}"; 
    }
}