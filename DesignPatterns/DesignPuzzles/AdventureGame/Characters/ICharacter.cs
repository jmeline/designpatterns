using DesignPatterns.DesignPuzzles.AdventureGame.ArmorBehaviors;
using DesignPatterns.DesignPuzzles.AdventureGame.WeaponBehaviors;

namespace DesignPatterns.DesignPuzzles.AdventureGame.Characters
{
    public abstract class Character 
    {
        public void SetWeapon(IWeapon newWeapon)
        {
            Weapon = newWeapon;
        }
        public void SetArmor(IArmor newArmor)
        {
            Armor = newArmor;
        }

        public void SetStats(IStats newStats)
        {
            
        }
        
        protected IStats Stats { get; set; }
        
        protected IWeapon Weapon { get; set; }
//        protected IArmor Armor { get; set; }
    }
}
private