namespace DesignPatterns.DesignPuzzles.AdventureGame.WeaponBehaviors
{
    public class Scythe : IWeapon
    {
        public string UseWeapon() => "Scythe";
    }
}