namespace DesignPatterns.DesignPuzzles.AdventureGame.WeaponBehaviors
{
    public class Dagger : IWeapon
    {
        public string UseWeapon() => "Dagger";
    }
}