namespace DesignPatterns.DesignPuzzles.AdventureGame.WeaponBehaviors
{
    public interface IWeapon
    {
        string Name { get; }
        int Damage { get; };
    }
}