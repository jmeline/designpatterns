namespace DesignPatterns.DesignPuzzles.AdventureGame.WeaponBehaviors
{
    public class Sword : IWeapon
    {
        public string Name => "Sword";
        public int Damage => 10;
    }
}