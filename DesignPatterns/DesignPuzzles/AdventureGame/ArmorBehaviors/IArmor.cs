namespace DesignPatterns.DesignPuzzles.AdventureGame.ArmorBehaviors
{
    public interface IArmor
    {
        string Name { get; set; }
        int GetArmorRating { get; set; }
    }
}