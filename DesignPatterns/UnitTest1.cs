using System;
using DesignPatterns.DesignPuzzles.AdventureGame.Characters;
using DesignPatterns.DesignPuzzles.AdventureGame.WeaponBehaviors;
using Shouldly;
using Xunit;

namespace DesignPatterns
{
    public class UnitTest1
    {
        [Fact]
        public void King()
        {
            var king = new King();
            king.GetRank().ShouldBe("I'm the king!");
            king.Swing().ShouldBe("Swong with a Sword");
            
            king.SetWeapon(new Scythe());
            king.Swing().ShouldBe("Swong with a Scythe");
        }
    }
}