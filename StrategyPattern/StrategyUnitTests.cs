using Shouldly;
using StrategyPattern.BadDuckPattern;
using StrategyPattern.BetterDuckPattern.Behaviors.Fly;
using StrategyPattern.BetterDuckPattern.Behaviors.Quack;
using Xunit;

namespace StrategyPattern
{
    public class StrategyUnitTests
    {
        [Fact]
        public void BadDuckPattern_DumbDuckTest()
        {
            /*
             * We're given a project that requires that we can simulate ducks.
             */
            
            // First off the design doc is that we need a few different ducks if we are going to
            // simulate their behavior. We begin by creating an abstract class "Duck" to represent all ducks. 
            // Each new duck that we want to simulate will inherit from "Duck"
            
            // We create a mallard and a ruddy duck. So far so good!
            var mallard = new MallardDuck();
            var ruddy = new RuddyDuck();
            
            // Design comes down and we need a way to make the ducks quack. So we add a "Quack" method to the superclass.
            // Since we add it to the superclass, each child class gets the methods.
            mallard.Quack().ShouldBe("Mallard: Quack!"); 
            ruddy.Quack().ShouldBe("Ruddy: Quack!");
            
            // Design comes down again and we need a way to make the ducks swim. So we add a "Swim" method
            mallard.Swim().ShouldBe("Mallard: Swim!"); 
            ruddy.Swim().ShouldBe("Ruddy: Swim!"); 
            
            // We then see a design come down requiring the ducks to be able to fly. Great, we'll add a "Fly" method
            mallard.Fly().ShouldBe("Mallard: Fly!"); 
            ruddy.Fly().ShouldBe("Ruddy: Fly!"); 
            
            ruddy.Display().ShouldBe("Looks like a Ruddy Duck");
            mallard.Display().ShouldBe("Looks like a Mallard Duck");
            
            // We're feeling pretty good about our design... 
        }
        
        [Fact]
        public void BadDuckPattern_RubberDuckIntroduced()
        {
            /*
             * One of the new executives proposes a need to be able to simulate a rubber duck
             */
            
            var rubber = new RubberDuck();
            
            // so far we see that our Duck design appears to fit the needs of the new proposal.
            rubber.Display().ShouldBe("Looks like a Rubber Duck");
            rubber.Swim().ShouldBe("RubberDuck: Swim!");
            
            // we begin to see that this rubber duck doesn't fit our original idea of what a duck is
            rubber.Fly().ShouldBe("RubberDuck: ...");
            rubber.Quack().ShouldBe("RubberDuck: ...");
            
            // rubber ducks don't quack or fly. So, we'll have to make exceptions for this duck. But we now have
            // inanimate objects flying in our simulator. What are we going to do?
            
            // Our choice of inheritance hasn't yielded us the best design and it has made it had adjusting to changes.
            
            // We could go down the route of adding a IFlyable or IQuackable to each duck class
            // and then handle the behavior from there
            
            // unfortunately this doesn't yield itself to maintainability as we would have to manually handle each class' implementation.
        }
        
        // The strategy pattern comes to the rescue as it enables us to encapsulate the duck behavior
        // and move it out of the duck classes. We have recognized that the fly and quack behaviors have been
        // more likely to vary due to the new cases we've been assigned to accomplish.

        // *****************************************
        // **Design Principle***
        // *****************************************
        // Identify the aspects of your application 
        // that vary and separate them from what
        // stays the same
        // *****************************************
        // Program to an interface, not an implementation
        // *****************************************
        [Fact]
        public void BetterDuckPattern_MallardDuck()
        {
            // Because we move the behavior into its own interface,
            // not only do we have a complete separation of concerns,
            // but those concerns can be modified at runtime.
            var mallard = new BetterDuckPattern.MallardDuck();
            
            mallard.performFly().ShouldBe("Fly with wings");
            mallard.performQuack().ShouldBe("Quack");
            
            
            // behavior modified
            
            mallard.ChangeFlyBehavior(new FlyNotAllowed());
            mallard.ChangeQuackBehavior(new QuietQuack());
            
            mallard.performFly().ShouldBe("Fly not allowed");
            mallard.performQuack().ShouldBe("Quiet Quack");
        }
        
        [Fact]
        public void BetterDuckPattern_RubberDuck()
        {
            var rubber = new BetterDuckPattern.RubberDuck();
            
            rubber.performFly().ShouldBe("Fly not allowed");
            rubber.performQuack().ShouldBe("...");
            
            // behavior modified
            
            rubber.ChangeFlyBehavior(new FlyWithWings());
            rubber.ChangeQuackBehavior(new QuietQuack());
            
            rubber.performFly().ShouldBe("Fly with wings");
            rubber.performQuack().ShouldBe("Quiet Quack");
        }
    }
}