using StrategyPattern.BetterDuckPattern.Behaviors.Fly;
using StrategyPattern.BetterDuckPattern.Behaviors.Quack;

namespace StrategyPattern.BetterDuckPattern
{
    public class RubberDuck : Duck
    {
        public RubberDuck()
        {
            _flyBehavior = new FlyNotAllowed();
            _quackBehavior = new MuteQuack();
        }
    }
}