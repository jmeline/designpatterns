using StrategyPattern.BetterDuckPattern.Behaviors.Fly;
using StrategyPattern.BetterDuckPattern.Behaviors.Quack;

namespace StrategyPattern.BetterDuckPattern
{
    public abstract class Duck
    {
        protected IFlyBehavior _flyBehavior;
        protected IQuackBehavior _quackBehavior;

        public void ChangeFlyBehavior(IFlyBehavior flyBehavior) => _flyBehavior = flyBehavior;
        public void ChangeQuackBehavior(IQuackBehavior quackBehavior) => _quackBehavior = quackBehavior;
        
        public string performQuack() => _quackBehavior.Speak();
        public string performFly() => _flyBehavior.Fly();
        
    }
}