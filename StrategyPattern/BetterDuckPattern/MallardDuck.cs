using StrategyPattern.BetterDuckPattern.Behaviors.Fly;
using StrategyPattern.BetterDuckPattern.Behaviors.Quack;

namespace StrategyPattern.BetterDuckPattern
{
    public class MallardDuck : Duck
    {
        public MallardDuck()
        {
            _flyBehavior = new FlyWithWings();
            _quackBehavior = new Quack();
        }

    }
}