namespace StrategyPattern.BetterDuckPattern.Behaviors.Fly
{
    public class FlyWithWings : IFlyBehavior
    {
        public string Fly() => "Fly with wings";
    }
}