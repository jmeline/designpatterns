namespace StrategyPattern.BetterDuckPattern.Behaviors.Fly
{
    public interface IFlyBehavior
    {
        string Fly();
    }
}