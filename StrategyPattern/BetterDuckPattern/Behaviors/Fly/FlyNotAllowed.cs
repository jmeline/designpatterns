namespace StrategyPattern.BetterDuckPattern.Behaviors.Fly
{
    public class FlyNotAllowed : IFlyBehavior
    {
        public string Fly() => "Fly not allowed";
    }
}