namespace StrategyPattern.BetterDuckPattern.Behaviors.Quack
{
    public class Quack : IQuackBehavior
    {
        public string Speak() => "Quack";
    }
}