namespace StrategyPattern.BetterDuckPattern.Behaviors.Quack
{
    public class MuteQuack : IQuackBehavior
    {
        public string Speak() => "...";
    }
}