namespace StrategyPattern.BetterDuckPattern.Behaviors.Quack
{
    public class QuietQuack : IQuackBehavior
    {
        public string Speak() => "Quiet Quack";
    }
}