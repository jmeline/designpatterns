namespace StrategyPattern.BetterDuckPattern.Behaviors.Quack
{
    public interface IQuackBehavior
    {
        string Speak();
    }
}