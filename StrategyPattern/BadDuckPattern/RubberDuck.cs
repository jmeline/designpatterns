namespace StrategyPattern.BadDuckPattern
{
    public class RubberDuck : BadDuckPattern.Duck
    {
        public override string Quack() => "RubberDuck: ...";

        public override string Swim() => "RubberDuck: Swim!";

        public override string Display() => "Looks like a Rubber Duck";

        public override string Fly() => "RubberDuck: ...";
    }
}