namespace StrategyPattern.BadDuckPattern
{
    public abstract class Duck
    {
        public abstract string Quack();

        public abstract string Swim();

        public abstract string Display();

        public abstract string Fly();
    }
}