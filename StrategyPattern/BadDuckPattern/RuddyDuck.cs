namespace StrategyPattern.BadDuckPattern
{
    public class RuddyDuck : BadDuckPattern.Duck
    {
        public override string Swim() => "Ruddy: Swim!";
        public override string Display() => "Looks like a Ruddy Duck";
        public override string Fly() => "Ruddy: Fly!";
        public override string Quack() => "Ruddy: Quack!";
    }
}