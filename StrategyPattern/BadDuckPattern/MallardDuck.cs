namespace StrategyPattern.BadDuckPattern
{
    public class MallardDuck : BadDuckPattern.Duck
    {
        public override string Swim() => "Mallard: Swim!";

        public override string Display() => "Looks like a Mallard Duck";
        public override string Fly() => "Mallard: Fly!";
        public override string Quack() => "Mallard: Quack!";
    }
}